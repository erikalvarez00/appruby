Rails.application.routes.draw do
  root 'zombies#index'

  devise_scope :admin do
  	get 'lista_usuarios/index'
  end

  devise_for :admins, controllers: {
  		sessions: 'admins/sessions'
  }
  
  devise_for :users, controllers: {
        sessions: 'users/sessions'
  }
  
  resources :zombies do
  	resources :brains
  end

  get '/cerebros',to:'brains#index',as:'brains'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
