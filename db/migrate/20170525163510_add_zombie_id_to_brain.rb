class AddZombieIdToBrain < ActiveRecord::Migration[5.0]
  def change
    add_column :brains, :zombie_id, :reference
  end
end
